

mod vec3;
mod scene;
mod camera;
mod mats;

use std::rc::Rc;
use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;
use image;
use vec3::{Vec3, Ray};
use scene::{Model, Sphere};
use camera::Camera;
use std::time::Instant;
use std::f32::consts::PI;

/// Given a filename, pixel data, and bounds, render a PNG image.

fn encode_png(filename: &str, pixels: &[Vec3], bounds: (usize, usize)) {
    let mut imgbuf =
        image::ImageBuffer::new(bounds.0 as u32, bounds.1 as u32);
    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let (x, y) = (x as usize, y as usize);
        *pixel = pixels[y*bounds.0 + x].to_rgb().unwrap();
    }
    let imgbuf = image::imageops::flip_vertical(&imgbuf);
    imgbuf.save(filename).unwrap();
}

const TAU: f32 = PI * 2.0;

fn main() {
    let imgx = 1600;
    let imgy = 900;
    let ns = 2;
    let mut rng = SmallRng::from_entropy();
    let mut pixels = vec![Vec3(0.0, 0.0, 0.0); imgx*imgy];
    let mut world: Vec<Box<dyn Model>> = Vec::with_capacity(200);
    world.push( Box::new(Sphere{
        center: Vec3(0.0,-400.0, -1.0),
        radius: 400.0,
        material: Rc::new(mats::Lambertian{ albedo: Vec3(0.4, 0.4, 0.6)}),
    }));
    for a in -7..8 {
        for b in -7..7 {
            let center = Vec3(1.2*a as f32 + 1.0*rng.gen::<f32>(), 0.2, 1.2*b as f32 + 1.0*rng.gen::<f32>());
            if (center - Vec3(4.0,0.2,0.0)).length() > 0.9 {
                    let sphere = Sphere{
                        center,
                        radius: 0.2,
                        material: match rng.gen::<f32>() {
                            x if x < 0.8 => Rc::new(mats::Lambertian{
                                albedo: Vec3(0.8*rng.gen::<f32>().powi(2), 0.8*rng.gen::<f32>().powi(2), 0.4+0.6*rng.gen::<f32>().powi(2))
                            }),
                            x if x < 0.95 => Rc::new(mats::Metal{
                                albedo: Vec3(0.4+0.4*rng.gen::<f32>(), 0.4+0.4*rng.gen::<f32>(), 0.6+0.4*rng.gen::<f32>(),),
                                fuzz: 0.3*rng.gen::<f32>(),
                            }),
                            x => Rc::new(mats::Dielectric{ref_idx: 1.2 + 0.8*rng.gen::<f32>()})
                        }
                    };
                world.push(Box::new(sphere));
            }
        }
    }
    world.push(Box::new(Sphere{
        center: Vec3(0.0, 1.0, 0.0),
        radius: 1.0,
        material: Rc::new(mats::Dielectric{ref_idx: 1.5})
    }));
    world.push(Box::new(Sphere{
        center: Vec3(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: Rc::new(mats::Lambertian{albedo: Vec3(0.4, 0.4, 0.8)}),
    }));
    world.push(Box::new(Sphere{
        center: Vec3(4.0, 1.0, 0.0),
        radius: 1.0,
        material: Rc::new(mats::Metal{albedo: Vec3(0.5, 0.5, 0.7), fuzz: 0.1}),
    }));
    let origin = Vec3(6.0, 1.5, 2.0);
    let look_at = Vec3(0.0, 0.0, 0.0);
    let cam = Camera::new(
        origin,
        look_at,
        Vec3(0.0, 1.0, 0.0),
        TAU / 4.0,
        imgx as f32 / imgy as f32,
        0.05,
        (look_at - origin).length(),
    );
    let start = Instant::now();
    for i in 0..imgx*imgy {
        let mut col = Vec3(0.0, 0.0, 0.0);
        for _ in 0..ns {
            let u = ((i % imgx) as f32 + rng.gen::<f32>()) / imgx as f32;
            let v = ((i / imgx) as f32 + rng.gen::<f32>()) / imgy as f32;
            let r = cam.get_ray(u, v);
            col = col + colour(&r, &world, 0);
        }
        col = col / ns as f32;
        col = Vec3(col.0.sqrt(), col.1.sqrt(), col.2.sqrt());
        pixels[i] = col;
    }
    encode_png("hello.png", &pixels, (imgx, imgy));
    println!("Rendered image in {0} ms", start.elapsed().as_millis());
}

const MAX_DEPTH: usize = 25;

fn colour(r: &Ray, model: &dyn Model, depth: usize) -> Vec3 {
    match model.hit(r) {
        Some(hit) => {
            let scatter = hit.material.scatter(r, &hit);
            match scatter.ray {
                Some(ray) if depth < MAX_DEPTH => scatter.attenuation*colour(&ray, model, depth + 1),
                _ => Vec3(0.0, 0.0, 0.0)
            }
        },
        None => {
            let u_direction = r.direction.unit();
            let t = 0.5 * (u_direction.1 + 1.0);
            (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0)
        }

    }
}
