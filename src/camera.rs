use crate::vec3::{Vec3, Ray};

pub struct Camera {
    origin: Vec3,
    lower_left: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    u: Vec3,
    v: Vec3,
    w: Vec3,
    lens_radius: f32,
}


impl Camera {
    pub fn new(origin: Vec3, look_at: Vec3, vec_up: Vec3, vfov: f32, aspect: f32, aperture: f32,
               foc_dist: f32) -> Self {
        let half_height = (vfov / 2.0).tan();
        let half_width = half_height * aspect;
        let w = (origin - look_at).unit();
        let u = vec_up.cross(w).unit();
        let v = w.cross(u);
        Self {
            origin,
            lower_left: origin - foc_dist*(half_width * u + half_height* v + w),
            horizontal: 2.0 * half_width * foc_dist * u,
            vertical: 2.0 * half_height * foc_dist * v,
            u, v, w,
            lens_radius: aperture / 2.0,
        }
    }

    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = Vec3::random_unit()*self.lens_radius;
        let offset = self.u * rd.0 + self.v * rd.1;
        Ray{
            origin: self.origin + offset,
            direction: self.lower_left + s * self.horizontal + t * self.vertical - self.origin - offset,
        }
    }
}