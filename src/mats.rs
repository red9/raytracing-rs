use std::time;
use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};
use crate::vec3::{Vec3, Ray};
use crate::scene::Hit;


pub struct Scatter {
    pub attenuation: Vec3,
    pub ray: Option<Ray>
}


pub trait Material {
    fn scatter(&self, ray_in: &Ray, hit: &Hit) -> Scatter;
}

pub struct Lambertian {
    pub albedo: Vec3,
}

impl Material for Lambertian {
    fn scatter(&self, _ray_in: &Ray, hit: &Hit) -> Scatter {
        let target = hit.p + hit.normal + 0.4*Vec3::random_unit();
        Scatter{
            attenuation: self.albedo,
            ray: Some(Ray::new(hit.p, target - hit.p))
        }
    }
}

fn reflect(v: Vec3, n: Vec3) -> Vec3 {
    v - 2.0*n*v.dot(n)
}

pub struct Metal {
    pub albedo: Vec3,
    pub fuzz: f32,
}

impl Material for Metal{
    fn scatter(&self, ray_in: &Ray, hit: &Hit) -> Scatter {
        let scattered = Ray{
            origin: hit.p,
            direction: reflect(ray_in.direction.unit(), hit.normal) + self.fuzz*Vec3::random_unit()
        };
        Scatter{
            attenuation: self.albedo,
            ray: match scattered {
                r if r.direction.dot(hit.normal) > 0.0 => Some(scattered),
                _r => None,
            },
        }
    }
}

pub struct Dielectric {
    pub ref_idx: f32
}

fn refract(v: Vec3, n: Vec3, ni_over_nt: f32) -> Option<Vec3> {
    let dt = n.dot(v.unit());
    let disc = 1.0 - ni_over_nt*ni_over_nt*(1.0-dt*dt);
    if disc > 0.0 {
        return Some(ni_over_nt*(v.unit() - n*dt) - n*disc.sqrt())
    } else { return None }
}

fn schlick(cosine: f32, ref_idx: f32) -> f32 {
    let r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
    let r0 = r0*r0;
    r0 + (1.0 - r0)*(1.0 - cosine).powi(5)
}

impl Material for Dielectric {
    fn scatter(&self, ray_in: &Ray, hit: &Hit) -> Scatter {
        let out_normal: Vec3;
        let ni_over_nt: f32;
        let cosine: f32;
        if ray_in.direction.dot(hit.normal) > 0.0 {
            out_normal = -hit.normal;
            ni_over_nt = self.ref_idx;
            cosine = self.ref_idx * ray_in.direction.dot(hit.normal) / ray_in.direction.length()
        } else {
            out_normal = hit.normal;
            ni_over_nt = 1.0 / self.ref_idx;
            cosine = -ray_in.direction.dot(hit.normal) / ray_in.direction.length();
        }
        let time_ns = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH).unwrap().subsec_nanos() as u64;
        let mut rng = SmallRng::seed_from_u64(time_ns);
        match refract(ray_in.direction, out_normal, ni_over_nt) {
            Some(refracted) if rng.gen::<f32>() > schlick(cosine, self.ref_idx) => Scatter{
                attenuation: Vec3(1.0, 1.0, 1.0),
                ray: Some(Ray::new(hit.p, refracted))
            },
            _ => Scatter{
                attenuation: Vec3(1.0, 1.0, 1.0),
                ray: Some(Ray::new(hit.p, reflect(ray_in.direction, hit.normal)))
            }
        }
    }
}