use std::ops::*;
use std::f32;
use std::time;
use image::Rgb;
use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;


#[derive(Debug, Clone, Copy)]
pub struct Vec3(pub f32, pub f32, pub f32);

impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3(x, y, z)
    }

    pub fn random_unit() -> Vec3 {
        let time_ns = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH).unwrap().subsec_nanos() as u64;
        let mut rng = SmallRng::seed_from_u64(time_ns);
        loop {
            let p = 2.0*Vec3(rng.gen(), rng.gen(), rng.gen()) - Vec3(1.0, 1.0, 1.0);
            if p.squared_length() < 1.0 {
                return p;
            }
        }
    }

    pub fn length(&self) -> f32 {
        (self.squared_length()).sqrt()
    }

    pub fn squared_length(&self) -> f32 {
        self.0 * self.0 + self.1 * self.1 + self.2 * self.2
    }

    pub fn dot(&self, other: Vec3) -> f32 {
        self.0 * other.0 + self.1 * other.1 + self.2 * other.2
    }

    pub fn cross(&self, other: Vec3) -> Vec3 {
        Vec3(
            self.1 * other.2 - self.2 * other.1,
            self.2 * other.0 - self.0 * other.2,
            self.0 * other.1 - self.1 * other.0,
        )
    }

    pub fn unit(self) -> Vec3 {
        self / self.length()
    }

    pub fn to_rgb(&self) -> Result<Rgb<u8>, String> {
        if self.0 < 0.0 || self.0 > 1.0 || self.1 < 0.0 || self.1 > 1.0 || self.2 < 0.0 || self.2 > 1.0 {
           return Err(format!("input out of bounds: {0} {1} {2}", self.0, self.1, self.2));
        }
        let r = (self.0 * 255.99) as u8;
        let g = (self.1 * 255.99) as u8;
        let b = (self.2 * 255.99) as u8;
        Ok(Rgb([r, g, b]))
    }
}

impl Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other:Vec3) -> Vec3 {
        Vec3(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}

impl Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, other:Vec3) -> Vec3 {
        Vec3(self.0 * other.0, self.1 * other.1, self.2 * other.2 )
    }
}

impl Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Vec3 {
        Vec3(-self.0, -self.1, -self.2)
    }
}

impl Mul<f32> for Vec3 {
    type Output = Vec3;

    fn mul(self, t: f32) -> Vec3 {
        Vec3(self.0 * t, self.1 * t, self.2 * t)
    }
}

impl Div<f32> for Vec3 {
    type Output = Vec3;

    fn div(self, t: f32) -> Vec3 {
        Vec3(self.0 / t, self.1 / t, self.2 / t)
    }
}

impl Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Vec3 {
        v*self
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin : Vec3, direction : Vec3) -> Ray {
        Ray{origin, direction}
    }

    pub fn point_at(&self, t: f32) -> Vec3 {
        self.origin + t*self.direction
    }
}