use std::rc::Rc;
use crate::vec3::{Vec3, Ray};
use crate::mats::Material;

/// `Hit` records information about a hit on an object.
/// `p` is the point at which an incoming `Ray` r hit the object,
/// such that r.point_at(`t`) = `p`.
/// `normal` is the surface normal of the object at the point at which we hit it.
#[derive(Clone)]
pub struct Hit {
    pub t: f32,
    pub p: Vec3,
    pub normal: Vec3,
    pub material: Rc<dyn Material>,
}

pub trait Model {
    // Does a given ray hit the object?
    fn hit(&self, r: &Ray) -> Option<Hit>;
}

pub struct Sphere {
    pub center: Vec3,
    pub radius: f32,
    pub material: Rc<dyn Material>,
}

const T_MIN: f32 = 0.0001;

impl Model for Sphere {
    // Does a given ray hit this sphere?
    fn hit(&self, r: &Ray) -> Option<Hit> {
        // a point p is *on* the sphere if (p-c).dot(p-c) = r^2. insert the ray's point_at function
        // into this and we can expand this into a quadratic equation and solve for t.
        let oc = r.origin - self.center;
        let a = r.direction.squared_length();
        let b = oc.dot(r.direction); // cancel twos
        let c = oc.squared_length() - self.radius*self.radius;
        let disc = b*b - a*c; // cancel twos
        if disc > 0.0 {
            let t = (-b - (b*b-a*c).sqrt())/a; //cancel twos
            if t > T_MIN {
                let hit = Hit{
                    t,
                    p: r.point_at(t),
                    normal: (r.point_at(t) - self.center) / self.radius,
                    material: self.material.clone()
                };
                return Some(hit);
            }
            let t = (-b + (b*b-a*c).sqrt())/a; //cancel twos
            if t > T_MIN {
                let hit = Hit{
                    t,
                    p: r.point_at(t),
                    normal: (r.point_at(t) - self.center) / self.radius,
                    material: self.material.clone()
                };
                return Some(hit);
            }
        }
        None
    }
}

impl Model for Vec<Box<dyn Model>> {
    fn hit(&self, r: &Ray) -> Option<Hit> {
        let mut best : Option<Hit> = None;
        for obj in self {
            if let Some(hit) = obj.hit(r) {
                match best.clone() {
                    Some(prev) if prev.t < hit.t => (),
                    _ => best = Some(hit)
                }
            }
        }
        best
    }
}

